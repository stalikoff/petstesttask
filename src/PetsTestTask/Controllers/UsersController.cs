﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using Microsoft.AspNetCore.Mvc;
using PetsTestTask.Repositories;
using PetsTestTask.Models;

namespace PetsTestTask.Controllers
{
    [Route("api/[controller]")]
    public class UsersController : Controller
    {
        private readonly IUsersRepository _usersRepository;
        private readonly IPetsRepository _petsRepository;

        public UsersController(IUsersRepository usersRepository, IPetsRepository petsRepository)
        {
            _usersRepository = usersRepository;
            _petsRepository = petsRepository;
        }

        // -------- Users ------------
        [HttpGet("")]
        public IEnumerable<User> GetAllUsers()
        {
            IEnumerable<User> users = _usersRepository.GetAllUsers();

            return users;
        }

        [HttpPost("")]
        public User AddUser([FromBody]User user)
        {
            if (user.Pets == null)
            {
                user.Pets = new List<Pet>();
            }

            User savedUser = _usersRepository.AddUser(user);

            return savedUser;
        }

        [HttpDelete("{userId}")]
        public void DeleteUser(int userId)
        {
            _usersRepository.DeleteUser(userId);
        }

        // ------------- Pets ---------------
        [HttpGet("{userId}/pets")]
        public IEnumerable<Pet> GetUserPets(int userId)
        {
            IEnumerable<Pet> pets = _petsRepository.GetUserPets(userId);

            return pets;
        }

        [HttpPost("{userId}/pets")]
        public Pet AddPet(int userId, [FromBody]Pet pet)
        {
            Pet savedPet = _petsRepository.AddPet(userId, pet);

            return savedPet;
        }

        [HttpDelete("{userId}/pets/{petId}")]
        public void DeletePet(int userId, int petId)
        {
            _petsRepository.DeletePet(userId, petId);
        }
        // ---------------------------------
    }
}
