﻿angular.module('petsModule', [])

     .service('PetsService', PetsService)

     .filter('startFrom', function () {
         return function (input, start) {
             start = +start;
             return input.slice(start);
         }
     })

    .component('petsComponent', {
        templateUrl: '../templates/pets.html',
        controller: petsController,
        bindings: { $router: '<' }
    });

function petsController($http, PetsService) {

    this.$routerOnActivate = function(data) {
        this.petsData = null;
        this.userId = data.params.id;
        this.Pet = {
            Id: '',
            Name: ''
        };
        PetsService.GetAllRecords(this.userId)
            .then(res => {
                this.petsData = res.data;
            }, err => {
                alert("Error : " + JSON.stringify(err.data));
            });
    };

    this.clear = () => {
        this.Pet.Id = '';
        this.Pet.Name = '';
    };

    // add new pet 
    this.addPet = () => {
        if (this.Pet.Name !== "") {
            $http({
                    method: 'POST',
                    url: '/api/users/' + this.userId + '/pets',
                    data: this.Pet
                })
                .then(res => {
                    this.petsData.push(res.data);
                    this.clear();
                    alert("Pet Added Successfully !!!");
                }, err => {
                    alert("Error : " + JSON.stringify(err.data));
                });
        }
    };

    // delete pet
    this.deletePet = index => {
        $http({
                method: 'DELETE',
                url: '/api/users/' + this.userId + '/pets/' + this.petsData[index].petId,
            })
            .then(() => {
                this.petsData.splice(index, 1);
                alert("Pet Deleted Successfully !!!");
            }, err => {
                alert("Error : " + JSON.stringify(err.data));
            });
    };

    this.navigateBack = () => {
        this.$router.navigate(['UsersModule']);
    }

    //---- pagination  ------
    this.currentPage = 0;
    this.itemsPerPage = 3;

    this.firstPage = () => {
        return this.currentPage === 0;
    }

    this.lastPage = () => {
        var lastPageNum = Math.ceil(this.petsData.length / this.itemsPerPage - 1);
        return this.currentPage === lastPageNum;
    }

    this.numberOfPages = () => {
        return Math.ceil(this.petsData.length / this.itemsPerPage);
    }

    this.startingItem = () => {
        return this.currentPage * this.itemsPerPage;
    }

    this.pageBack = () => {
        this.currentPage = this.currentPage - 1;
    }

    this.pageForward = () => {
        this.currentPage = this.currentPage + 1;
    }
    // ---------------------
}

function PetsService($http) {
    return {
        GetAllRecords: userId => {
            return $http.get('/api/users/' + userId + '/pets');
        }
    }
}