﻿angular.module('appModule', ['ngComponentRouter', 'usersModule', 'petsModule'])

.value('$routerRootComponent', 'app')

.component('app', {
    template: '<ng-outlet></ng-outlet>',
    $routeConfig: [
      { path: '/users', name: 'UsersModule', component: 'usersComponent', useAsDefault: true },
      { path: '/users/:id', name: 'PetsModule', component: 'petsComponent' }
    ]
});