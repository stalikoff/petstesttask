﻿angular.module('usersModule', ['petsModule'])

    .service('UsersService', UsersService)

    .filter('startFrom', function () {
        return function (input, start) {
            start = +start;
            return input.slice(start);
        }
    })

    .component('usersComponent', {
        templateUrl: '../templates/users.html',
        controller: usersController,
        bindings: { $router: '<' }
    });

function usersController ($http, UsersService) {

    this.$routerOnActivate = function(data) {
        this.usersData = null;

        this.User = {
            Id: '',
            Name: '',
            Pets: ''
        };

        UsersService.GetAllRecords()
            .then(res => {
                this.usersData = res.data;
            }, err => {
                alert("Error : " + JSON.stringify(err.data));
            });
    };

    this.clear = () => {
        this.User.Id = '';
        this.User.Name = '';
        this.User.Pets = '';
    };

    // add new user 
    this.addUser = () => {
        if (this.User.Name !== "") {
            $http({
                    method: 'POST',
                    url: '/api/users',
                    data: this.User
                })
                .then(res => {
                    this.usersData.push(res.data);
                    this.clear();
                    alert("User Added Successfully !!!");
                }, err => {
                    alert("Error : " + JSON.stringify(err.data));
                });
        }
    };

    // delete user
    this.deleteUser = (index) => {
        $http({
                method: 'DELETE',
                url: '/api/users/' + this.usersData[index].userId,
            })
            .then(() => {
                this.usersData.splice(index, 1);
                alert("User Deleted Successfully !!!");
            }, err => {
                alert("Error : " + JSON.stringify(err.data));
            });
    };

    this.editPets = (userId) => {
        this.$router.navigate(['PetsModule', {id: userId}]);
    };

    //---- pagination  ------
    this.currentPage = 0;
    this.itemsPerPage = 3;

    this.firstPage = () => {
        return this.currentPage === 0;
    }

    this.lastPage = () => {
        var lastPageNum = Math.ceil(this.usersData.length / this.itemsPerPage - 1);
        return this.currentPage === lastPageNum;
    }

    this.numberOfPages = () => {
        return Math.ceil(this.usersData.length / this.itemsPerPage);
    }

    this.startingItem = () => {
        return this.currentPage * this.itemsPerPage;
    }

    this.pageBack = () => {
        this.currentPage = this.currentPage - 1;
    }

    this.pageForward = () => {
        this.currentPage = this.currentPage + 1;
    }
    // ---------------------
}

function UsersService ($http) {
    return {
        GetAllRecords: () => {
            return $http.get('/api/users');
        }
    };
}