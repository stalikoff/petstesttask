﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace PetsTestTask.Models
{
    public class User
    {
        public int UserId { get; set; }
        public string Name { get; set; }

        public virtual ICollection<Pet> Pets { get; set; }

        public User()
        {
            Pets = new List<Pet>();
        }
    }
}
