﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace PetsTestTask.Models
{
    public class Pet
    {
        public int PetId { get; set; }
        public string Name { get; set; }

        public int UserId { get; set; }
        public virtual User User { get; set; }

        public Pet()
        { }
    }
}
