﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using PetsTestTask.Models;

namespace PetsTestTask.Repositories
{
    public interface IPetsRepository
    {
        IEnumerable<Pet> GetUserPets(int userId);
        Pet AddPet(int userId, Pet pet);
        void DeletePet(int userId, int petId);
    }
}
