﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using PetsTestTask.Models;

namespace PetsTestTask.Repositories
{
    public interface IUsersRepository
    {
        IEnumerable<User> GetAllUsers();
        User AddUser(User user);
        void DeleteUser(int userId);
    }
}
