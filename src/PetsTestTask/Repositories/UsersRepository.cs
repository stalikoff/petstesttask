﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using PetsTestTask.Models;
using Microsoft.EntityFrameworkCore;

namespace PetsTestTask.Repositories
{
    public class UsersRepository : IUsersRepository
    {
        private readonly DatabaseContext _dbContext;

        public UsersRepository(DatabaseContext dbContext)
        {
            _dbContext = dbContext;
        }

        public IEnumerable<User> GetAllUsers()
        {
            IEnumerable<User> users = _dbContext.Users
                .Include(u => u.Pets);

            return users;
        }

        public User AddUser(User user)
        {
            _dbContext.Users.Add(user);
            _dbContext.SaveChanges();

            return user;
        }

        public void DeleteUser(int userId)
        {
            User user = _dbContext.Users
                .First(u => u.UserId == userId);

            if (user != null)
            {
                _dbContext.Users.Remove(user);
                _dbContext.SaveChanges();
            }
        }
    }
}