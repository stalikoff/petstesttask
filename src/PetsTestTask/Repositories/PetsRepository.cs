﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using PetsTestTask.Models;
using Microsoft.EntityFrameworkCore;

namespace PetsTestTask.Repositories
{
    public class PetsRepository : IPetsRepository
    {
        private readonly DatabaseContext _dbContext;

        public PetsRepository(DatabaseContext dbContext)
        {
            _dbContext = dbContext;
        }

        public IEnumerable<Pet> GetUserPets(int userId)
        {
            User user = _dbContext.Users
                .Include(u => u.Pets)
                .First(u => u.UserId == userId);

            return user.Pets;
        }

        public Pet AddPet(int userId, Pet pet)
        {
            User user = _dbContext.Users
                .Include(u => u.Pets)
                .First(u => u.UserId == userId);

            user.Pets.Add(pet);
            _dbContext.SaveChanges();

            return pet;
        }

        public void DeletePet(int userId, int petId)
        {
            User user = _dbContext.Users
                .Include(u => u.Pets)
                .First(u => u.UserId == userId);

            Pet pet = user.Pets
                .First(p => p.PetId == petId);

            user.Pets.Remove(pet);

            _dbContext.SaveChanges();
        }
    }
}
